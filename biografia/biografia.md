# Joshtyn Zambrano
<img src="https://gitlab.com/jjjzambrano/capacitaciones-yv-mlab-2021/-/raw/master/Imagenes/foto.png " width = "125">

Nací en Quito en 1999, tengo 21 años, mis padres son Jhon Zambrano y Tatiana Jácome, soy una persona amable y respetuosa, Fui criado en una familia con buenos valores. Estudie 13 años en la Unidad Educativa Particular “FESVIP” 

Mis pasatiempos son escuchar música, jugar videojuegos, salir al parque junto con mi familia y estar aprendiendo varias cosas interesantes.

Mis metas a corto plazo son es pasar al siguiente nivel de mi carrera, aprender más sobre lo que estoy aprendiendo. Mis metas a largo plazo son en convertirme en un desarrollador de software, conseguir un buen trabajo aplicando lo que he aprendido y mejorar mis conocimientos.
